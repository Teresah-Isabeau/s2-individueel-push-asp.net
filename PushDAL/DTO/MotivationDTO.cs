﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public class MotivationDTO
    {      
        public int MotivationID { get; set; }
        public string Text { get; set; }
        public bool Positive { get; set; }
        

        public MotivationDTO(string text, bool positive)
        {
            this.Text = text;
            this.Positive = positive;
        }

        public MotivationDTO(int motivationID, string text, bool positive)
        {
            this.MotivationID = motivationID;
            this.Text = text;
            this.Positive = positive;
        }

        
    }
}
