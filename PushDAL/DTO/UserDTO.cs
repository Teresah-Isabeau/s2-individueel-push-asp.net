﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public class UserDTO
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Length { get; set; }
        public DateTime Age { get; set; }
        public string Gender { get; set; }
        public int Admin { get; set; }
        public double Goalweight { get; set; }
       

        public UserDTO(string email)
        {
            this.Email = email;
        }

        public UserDTO(int userID)
        {
            this.UserID = userID;
        }

        public UserDTO(string email, string password)
        {
            this.Email = email;
            this.Password = password;
        }

        public UserDTO(string name, string email, string password, int length)
        {
            this.Name = name;
            this.Email = email;
            this.Password = password;
            this.Length = length;
        }

        public UserDTO(int userid, string name, string email, string password, int length, DateTime age, string gender, int admin, double goalweight)
        {
            this.UserID = userid;
            this.Name = name;
            this.Email = email;
            this.Password = password;
            this.Length = length;
            this.Age = age;
            this.Gender = gender;
            this.Admin = admin;
            this.Goalweight = goalweight;
        }
    }
}
