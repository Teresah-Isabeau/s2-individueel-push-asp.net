﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public class TipDTO
    {
        public int TipID { get; set; }

        public string Tip { get; set; }

        public TipDTO(string tip)
        {
            this.Tip = tip;
        }

        public TipDTO(int tipID, string tip)
        {
            this.TipID = tipID;
            this.Tip = tip;
        }

        
    }
}
