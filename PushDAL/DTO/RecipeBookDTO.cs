﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public class RecipeBookDTO
    {
        public int RecipeBookID { get; set; }
        public string RecipeType { get; set; }

        public RecipeBookDTO(int recipebookID, string recipeType)
        {
            this.RecipeBookID = recipebookID;
            this.RecipeType = recipeType;
        }
    }
}
