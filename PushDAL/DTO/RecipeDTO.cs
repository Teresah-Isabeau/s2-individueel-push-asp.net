﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public class RecipeDTO
    {
        public int RecipeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int RecipeBookID { get; set; }

        public RecipeDTO(int recipeID, string name, string description, int recipeBookID)
        {
            this.RecipeID = recipeID;
            this.Name = name;
            this.Description = description;
            this.RecipeBookID = recipeBookID;
        }
    }
}
