﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public interface IRecipeBookContainer
    {
        List<RecipeBookDTO> GetAllTypes();
    }
}
