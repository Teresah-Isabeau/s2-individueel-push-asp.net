﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public interface ITipContainer
    {
        public TipDTO RandomTip();
        bool EditTip(TipDTO tipDTO);
        bool DeleteTip(TipDTO tipDTO);
        TipDTO GetSelectedTip(int tipID);
        List<TipDTO> GetAllTips();
    }
}
