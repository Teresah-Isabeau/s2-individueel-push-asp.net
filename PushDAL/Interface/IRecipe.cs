﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public interface IRecipe
    {
        bool AddRecipe(RecipeDTO recipeDTO);
        bool EditRecipe(RecipeDTO recipeDTO);
        bool DeleteRecipe(RecipeDTO recipeDTO);
        RecipeDTO GetSelectedRecipe(int recipeID);
    }
}
