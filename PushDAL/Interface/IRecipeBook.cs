﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public interface IRecipeBook
    {
        RecipeBookDTO GetSelectedRecipe(int recipebookID);
    }
}
