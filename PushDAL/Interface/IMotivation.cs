﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public interface IMotivation
    {
        bool AddMotivation(MotivationDTO MotivationDTO);
        bool EditMotivation(MotivationDTO MotivationDTO);
        bool DeleteMotivation(MotivationDTO MotivationDTO);
        public MotivationDTO RandomPositiveMotivation();
        public MotivationDTO RandomNegativeMotivation();
        public MotivationDTO GetSelectedMotivation(int MotivationID);
    }
}
