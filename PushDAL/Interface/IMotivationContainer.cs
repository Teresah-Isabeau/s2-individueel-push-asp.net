﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public interface IMotivationContainer
    {
        List<MotivationDTO> GetAllMotivations();
    }
}
