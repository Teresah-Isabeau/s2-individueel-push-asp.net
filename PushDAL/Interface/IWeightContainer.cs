﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public interface IWeightContainer
    {
        bool EditWeight(WeightDTO weightDTO);
        bool DeleteWeight(WeightDTO weightDTO);
        List<WeightDTO> GetAllWeight(int userID);
        WeightDTO GetFirstWeight(int userID);
        WeightDTO GetLastWeight(int userId);
        WeightDTO GetSelectedWeight(int userID, int WeightID);
        WeightDTO WeightResult(int userID);
    }
}
