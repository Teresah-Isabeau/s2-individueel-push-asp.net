﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PushDAL
{
    public interface IRecipeContainer
    {
        List<RecipeDTO> GetAllRecipes(int recipebookID);
    }
}
