﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace PushDAL
{
    public interface IUserContainer
    {
        bool EditUser(UserDTO userDTO);
        bool DeleteUser(UserDTO userDTO);
        bool CheckUserExists(UserDTO userDTO);
        bool CheckUniqueEmail(UserDTO userDTO);
        public UserDTO FetchUser(UserDTO userDTO);
        public List<UserDTO> GetAllUsers();
        UserDTO GetSelectedUser(int userID);
        bool ResetPassword(UserDTO userDTO);
        
    }
}
