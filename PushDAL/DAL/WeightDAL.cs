﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows;

namespace PushDAL
{
    public class WeightDAL : Database, IWeight, IWeightContainer
    {
        public bool AddWeight(WeightDTO weightDTO)
        {        
            SqlCommand command = new SqlCommand("dbo.AddNewWeight", this.sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", weightDTO.UserID);
            command.Parameters.AddWithValue("@Weight", weightDTO.WeightAmount);
            command.Parameters.AddWithValue("@WeightDate", weightDTO.WeightDate);
            return ExecuteBool(command);
        }

        public bool EditWeight(WeightDTO weightDTO)
        {
            SqlCommand command = new SqlCommand("UPDATE [dbo].[Weight] SET Weight = @Weight, WeightDate = @WeightDate WHERE UserID = @UserID AND WeightID = @WeightID", sqlConnection);
            command.Parameters.AddWithValue("@UserID", weightDTO.UserID);
            command.Parameters.AddWithValue("@WeightID", weightDTO.WeightID);
            command.Parameters.AddWithValue("@Weight", weightDTO.WeightAmount);
            command.Parameters.AddWithValue("@WeightDate", weightDTO.WeightDate);
            return ExecuteBool(command);
        }

        public bool DeleteWeight(WeightDTO weightDTO)
        {
            SqlCommand command = new SqlCommand("Delete FROM [dbo].[Weight] WHERE UserID = @UserID AND WeightID = @WeightID", sqlConnection);
            command.Parameters.AddWithValue("@UserID", weightDTO.UserID);
            command.Parameters.AddWithValue("@WeightID", weightDTO.WeightID);        
            return ExecuteBool(command);
        }

        //haal de gewichten op van 1 gebruiker
        public List<WeightDTO> GetAllWeight(int userID)
        {
            Open();
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[Weight] WHERE UserID = @userID ORDER BY WeightDate", sqlConnection);
            //command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userID);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            
            try
            {
                List<WeightDTO> weights = new List<WeightDTO>();

                foreach (DataRow row in table.Rows)
                {
                    WeightDTO weight = new WeightDTO(
                        Convert.ToInt32(row["UserID"]),
                        Convert.ToInt32(row["WeightID"]),
                        Convert.ToDouble(row["Weight"]),
                        Convert.ToDateTime(row["WeightDate"])
                        );
                    weights.Add(weight);
                }
                return weights;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Close();
            }
        }

        //Eerste gewicht ophalen van gebruiker
        public WeightDTO GetFirstWeight(int userID)
        {
            SqlCommand command = new SqlCommand("SELECT TOP 1 * FROM [dbo].[Weight] WHERE UserID = @userID ORDER BY WeightDate", sqlConnection);
            command.Parameters.AddWithValue("@UserID", userID);
            DataRow row = ExecuteRow(command);
            if(row == null)
            {
                return new WeightDTO(
                       userID,
                       0,
                       0,
                       default);
            }
            else
            {
                return new WeightDTO(
                       Convert.ToInt32(row["UserID"]),
                       Convert.ToInt32(row["WeightID"]),
                       Convert.ToDouble(row["Weight"]),
                       Convert.ToDateTime(row["WeightDate"]));
            }   
        }

        public WeightDTO GetLastWeight(int userID)
        {
            SqlCommand command = new SqlCommand("SELECT TOP 1 * FROM [dbo].[Weight] WHERE UserID = @userID ORDER BY WeightDate DESC", sqlConnection);
            command.Parameters.AddWithValue("@UserID", userID);
            DataRow row = ExecuteRow(command);

            if (row == null)
            {
                return new WeightDTO(
                       userID,
                       0,
                       0,
                       default);
            }
            else
            {
                return new WeightDTO(
                       Convert.ToInt32(row["UserID"]),
                       Convert.ToInt32(row["WeightID"]),
                       Convert.ToDouble(row["Weight"]),
                       Convert.ToDateTime(row["WeightDate"]));
            }
        }

        public WeightDTO GetSelectedWeight(int userID, int weightID)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[Weight] WHERE UserID = @userID AND WeightID = @WeightID", sqlConnection);
            command.Parameters.AddWithValue("@UserID", userID);
            command.Parameters.AddWithValue("@WeightID", weightID);
            DataRow row = ExecuteRow(command);

            return new WeightDTO(
                       Convert.ToInt32(row["UserID"]),
                       Convert.ToInt32(row["WeightID"]),
                       Convert.ToDouble(row["Weight"]),
                       Convert.ToDateTime(row["WeightDate"]));
        }

        public WeightDTO WeightResult(int userID)
        {
            SqlCommand command = new SqlCommand("SELECT TOP 2 * FROM [dbo].[Weight] WHERE UserID = @userID ORDER BY WeightDate DESC", sqlConnection);
            command.Parameters.AddWithValue("@UserID", userID);
            DataRow row = ExecuteRow(command);

            return new WeightDTO(
                       Convert.ToInt32(row["UserID"]),
                       Convert.ToDouble(row["Weight"]),
                       Convert.ToDateTime(row["WeightDate"]));
        }
    }
}
