﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PushDAL
{
    public abstract class Database
    {
        private string connectionString = @"Server = mssql.fhict.local; Database = dbi459672; User Id = dbi459672; Password = Pusheen2021!;";

        public SqlConnection sqlConnection;

        public Database()
        {
            sqlConnection = new SqlConnection(connectionString);
        }

        public void Open()
        {
            try
            {
                sqlConnection.Open();
            }
            catch (Exception ex)
            {
                throw ex;   
            }
        }

        public void Close()
        {
            sqlConnection.Close();
        }

        public bool ExecuteBool(SqlCommand command)
        {
            Open();
            try
            {
                SqlDataReader reader = command.ExecuteReader();
                return reader.HasRows;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                Close();
            }
        }

        public DataRow ExecuteRow(SqlCommand command)
        {
            Open();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            try
            {
                if (table.Rows.Count > 0)
                {
                    DataRow row = table.Rows[0];
                    return row;
                }
                //throw new Exception("Couldn't find Data");
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Close();
            }
        }

    }
}
