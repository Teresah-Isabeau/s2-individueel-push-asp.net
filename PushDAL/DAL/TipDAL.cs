﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows;

namespace PushDAL
{
    public class TipDAL : Database, ITip, ITipContainer
    {
        public bool AddTip(TipDTO tipDTO)
        {
            SqlCommand command = new SqlCommand("INSERT INTO [dbo].[Tips] (Text) VALUES (@Text)", sqlConnection);
            command.Parameters.AddWithValue("@Text", tipDTO.Tip);
            return ExecuteBool(command);
        }

        public bool EditTip(TipDTO tipDTO)
        {
            SqlCommand command = new SqlCommand("UPDATE [dbo].[Tips] SET Text = @Text WHERE TipsID = @TipsID", sqlConnection);
            command.Parameters.AddWithValue("TipsID", tipDTO.TipID);
            command.Parameters.AddWithValue("@Text", tipDTO.Tip);
            return ExecuteBool(command);
        }

        public bool DeleteTip(TipDTO tipDTO)
        {
            SqlCommand command = new SqlCommand("Delete FROM [dbo].[Tips] WHERE TipsID = @TipsID", sqlConnection);
            command.Parameters.AddWithValue("@TipsID", tipDTO.TipID);
            return ExecuteBool(command);
        }

        public TipDTO RandomTip()
        {
            SqlCommand command = new SqlCommand("SELECT TOP 1 * FROM [dbo].[Tips] ORDER BY NEWID()", sqlConnection);
            DataRow row = ExecuteRow(command);
            return new TipDTO(row["Text"].ToString());
        }

        public List<TipDTO> GetAllTips()
        {
            Open();
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[Tips]", sqlConnection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            try
            {
                List<TipDTO> Tips = new List<TipDTO>();

                foreach (DataRow row in table.Rows)
                {
                    TipDTO tipsDTO = new TipDTO(
                        Convert.ToInt32(row["TipsID"]),
                        Convert.ToString(row["Text"])
                        );
                    Tips.Add(tipsDTO);
                }
                return Tips;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Close();
            }
        }

        public TipDTO GetSelectedTip(int tipID)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[User] WHERE TipsID = @TipsID", sqlConnection);
            command.Parameters.AddWithValue("@TipsID", tipID);
            DataRow row = ExecuteRow(command);

            return new TipDTO(
                        Convert.ToInt32(row["TipsID"]),
                        Convert.ToString(row["Text"]));
        }
    }
}
