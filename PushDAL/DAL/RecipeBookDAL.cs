﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PushDAL
{
    public class RecipeBookDAL : Database, IRecipeBook, IRecipeBookContainer
    {
        public List<RecipeBookDTO> GetAllTypes()
        {
            Open();
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[RecipeBook]", sqlConnection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            try
            {
                List<RecipeBookDTO> Recipebooks = new List<RecipeBookDTO>();

                foreach (DataRow row in table.Rows)
                {
                    RecipeBookDTO recipeBookDTO = new RecipeBookDTO(
                        Convert.ToInt32(row["RecipeBookID"]),
                        Convert.ToString(row["RecipeType"])                      
                        );
                    Recipebooks.Add(recipeBookDTO);
                }
                return Recipebooks;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Close();
            }
        }

        public RecipeBookDTO GetSelectedRecipe(int recipebookID)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[Recipe] WHERE RecipeID = @RecipeID", sqlConnection);
            command.Parameters.AddWithValue("@RecipeID", recipebookID);
            DataRow row = ExecuteRow(command);

            return new RecipeBookDTO(
                        Convert.ToInt32(row["RecipeBookID"]),
                        Convert.ToString(row["RcipeType"]));
        }
    }
}
