﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows;

namespace PushDAL
{
    public class UserDAL : Database, IUser, IUserContainer
    {
        /// <summary>
        /// Voegt een gebruiker toe aan de database
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public bool AddUser(UserDTO userDTO)
        {
            SqlCommand command = new SqlCommand("dbo.CreateNewUser", this.sqlConnection);
            command.CommandType = CommandType.StoredProcedure;            
            command.Parameters.AddWithValue("@Email", userDTO.Email);
            command.Parameters.AddWithValue("@Password", userDTO.Password);
            command.Parameters.AddWithValue("@Name", userDTO.Name);
            command.Parameters.AddWithValue("@Length", userDTO.Length);
            return ExecuteBool(command);    
        }

        public bool EditUser(UserDTO userDTO)
        {
            SqlCommand command = new SqlCommand("UPDATE [dbo].[User] SET Email = @Email, Password = @Password, Name = @Name, Length = @Length WHERE UserID = @UserID", sqlConnection);           
            command.Parameters.AddWithValue("@Email", userDTO.Email);
            command.Parameters.AddWithValue("@Password", userDTO.Password);
            command.Parameters.AddWithValue("@Name", userDTO.Name);
            command.Parameters.AddWithValue("@Length", userDTO.Length);
            return ExecuteBool(command);
        }

        public bool DeleteUser(UserDTO userDTO)
        {
            SqlCommand command = new SqlCommand("SELECT WeightID * FROM [dbo].[Weight] WHERE UserID = @UserID, DELETE FROM [dbo].[weight] WHERE WeightID = @WeightID AND UserID = @UserID, DELETE FROM [dbo].[User] WHERE UserID = @UserID", sqlConnection);
            command.Parameters.AddWithValue("@UserID", userDTO.UserID);
            return ExecuteBool(command);
        }

        /// <summary>
        /// Haalt de userinfo op uit de database
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public UserDTO FetchUser(UserDTO userDTO)
        {  
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[User] WHERE Email = @Email AND Password = @Password", sqlConnection);
            command.Parameters.AddWithValue("@Email", userDTO.Email);
            command.Parameters.AddWithValue("@Password", userDTO.Password);
            DataRow row = ExecuteRow(command);

            userDTO.UserID = Convert.ToInt32(row["UserID"].ToString());
            userDTO.Name = row["Name"].ToString();
            userDTO.Email = row["Email"].ToString();
            userDTO.Password = row["Password"].ToString();
            userDTO.Length = Convert.ToInt32(row["Length"].ToString());
            //userDTO.Age = Convert.ToDateTime(row["Age"].ToString());
            if(row["Age"].ToString() != "")
            {
                userDTO.Age = Convert.ToDateTime(row["Age"].ToString());
            }
            userDTO.Gender = row["Gender"].ToString();

            userDTO.Admin = 0;
            if (row["Admin"].ToString()== "1")
            {
                userDTO.Admin = 1;
            }

            if (row["Goalweight"].ToString() != "")
            {
                userDTO.Goalweight = Convert.ToDouble(row["Goalweight"].ToString());
            }

            return userDTO;           
        }

        public List<UserDTO> GetAllUsers()
        {
            Open();
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[User]", sqlConnection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            try
            {
                List<UserDTO> users = new List<UserDTO>();

                foreach(DataRow row in table.Rows)
                {
                    UserDTO userDTO = new UserDTO(
                        Convert.ToInt32(row["UserID"]),
                        Convert.ToString(row["Name"]),
                        Convert.ToString(row["Email"]),
                        Convert.ToString(row["Password"]),
                        Convert.ToInt32(row["Length"]),
                        Convert.ToDateTime(row["Age"]),
                        Convert.ToString(row["Gender"]),
                        Convert.ToInt32(row["Admin"]),
                        Convert.ToDouble(row["Goalweight"])
                        );
                    users.Add(userDTO);
                }
                return users;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                Close();
            }
        }

        public UserDTO GetSelectedUser(int userID)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[User] WHERE UserID = @userID", sqlConnection);
            command.Parameters.AddWithValue("@UserID", userID);
            DataRow row = ExecuteRow(command);

            return new UserDTO(
                        Convert.ToInt32(row["UserID"]),
                        Convert.ToString(row["Name"]),
                        Convert.ToString(row["Email"]),
                        Convert.ToString(row["Password"]),
                        Convert.ToInt32(row["Length"]),
                        Convert.ToDateTime(row["Age"]),
                        Convert.ToString(row["Gender"]),
                        Convert.ToInt32(row["Admin"]),
                        Convert.ToDouble(row["Goalweight"]));
        }

        /// <summary>
        /// Checkt of de gebruiker bestaat in de database
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public bool CheckUserExists(UserDTO userDTO)
        {
            SqlCommand command = new SqlCommand("dbo.CheckUserExists", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Email", userDTO.Email);
            command.Parameters.AddWithValue("@Password", userDTO.Password);
            return ExecuteBool(command);
        }

        /// <summary>
        /// Checkt of ingevulde email al bestaat in de database
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public bool CheckUniqueEmail(UserDTO userDTO)
        {
            SqlCommand command = new SqlCommand("dbo.CheckUniqueEmail", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Email", userDTO.Email);
            return ExecuteBool(command);
        }

        /// <summary>
        /// Reset het wachtwoord
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        public bool ResetPassword(UserDTO userDTO)
        {
            SqlCommand command = new SqlCommand("UPDATE [dbo].[User] SET Password = @Password WHERE UserID = @UserID", sqlConnection);
            command.Parameters.AddWithValue("@UserID", userDTO.UserID);
            command.Parameters.AddWithValue("@Password", userDTO.Password);
            return ExecuteBool(command);
        }
    }
}
