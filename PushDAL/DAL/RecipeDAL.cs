﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PushDAL
{
    public class RecipeDAL : Database, IRecipe, IRecipeContainer
    {
        public bool AddRecipe(RecipeDTO recipeDTO)
        {
            SqlCommand command = new SqlCommand("INSERT INTO [dbo].[Recipe] ([Name], [Description], [RecipeBookID]) VALUES (@Name, @Description, @RecipeBookID)", sqlConnection);
            command.Parameters.AddWithValue("@Name", recipeDTO.Name);
            command.Parameters.AddWithValue("@Description", recipeDTO.Description);
            command.Parameters.AddWithValue("@RecipeBookID", recipeDTO.RecipeBookID);
            return ExecuteBool(command);
        }

        public bool EditRecipe(RecipeDTO recipeDTO)
        {
            SqlCommand command = new SqlCommand("UPDATE [dbo].[Recipe] SET Name = @Name, Description = @Description, RecipeBookID = @RecipeBookID WHERE RecipeID = @RecipeID", sqlConnection);
            command.Parameters.AddWithValue("RecipeID", recipeDTO.RecipeID);
            command.Parameters.AddWithValue("@Name", recipeDTO.Name);
            command.Parameters.AddWithValue("@Description", recipeDTO.Description);
            command.Parameters.AddWithValue("@RecipeBookID", recipeDTO.RecipeBookID);
            return ExecuteBool(command);
        }

        public bool DeleteRecipe(RecipeDTO recipeDTO)
        {
            SqlCommand command = new SqlCommand("Delete FROM [dbo].[Recipe] WHERE RecipeID = @RecipeID", sqlConnection);
            command.Parameters.AddWithValue("@RecipeID", recipeDTO.RecipeID);
            return ExecuteBool(command);
        }

        public List<RecipeDTO> GetAllRecipes(int recipebookID)
        {
            Open();
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[Recipe] WHERE RecipeBookID = @RecipeBookID", sqlConnection);
            command.Parameters.AddWithValue("@RecipeBookID", recipebookID);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            try
            {
                List<RecipeDTO> Recipes = new List<RecipeDTO>();

                foreach (DataRow row in table.Rows)
                {
                    RecipeDTO recipeDTO = new RecipeDTO(
                        Convert.ToInt32(row["RecipeID"]),
                        Convert.ToString(row["Name"]),
                        Convert.ToString(row["Description"]),
                        Convert.ToInt32(row["RecipeBookID"])
                        );
                    Recipes.Add(recipeDTO);
                }
                return Recipes;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Close();
            }
        }

        public RecipeDTO GetSelectedRecipe(int recipeID)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[Recipe] WHERE RecipeID = @RecipeID", sqlConnection);
            command.Parameters.AddWithValue("@RecipeID", recipeID);
            DataRow row = ExecuteRow(command);

            return new RecipeDTO(
                        Convert.ToInt32(row["RecipeID"]),
                        Convert.ToString(row["Name"]),
                        Convert.ToString(row["Description"]),
                        Convert.ToInt32(row["RecipeBookID"]));
        }
    }
}

