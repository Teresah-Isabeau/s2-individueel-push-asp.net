﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace PushDAL
{
    public class MotivationDAL : Database, IMotivation, IMotivationContainer
    {
        public bool AddMotivation(MotivationDTO MotivationDTO)
        {
            SqlCommand command = new SqlCommand("INSERT INTO [dbo].[Motivation] ([Text],[Positive]) VALUES (@Text, @Positive)", sqlConnection);
            command.Parameters.AddWithValue("@Text", MotivationDTO.Text);
            return ExecuteBool(command);
        }

        public bool EditMotivation(MotivationDTO MotivationDTO)
        {
            SqlCommand command = new SqlCommand("UPDATE [dbo].[Motivation] SET Text = @Text, Positive = @Positive WHERE MotivationID = @MotivationID", sqlConnection);
            command.Parameters.AddWithValue("MotivationID", MotivationDTO.MotivationID);
            command.Parameters.AddWithValue("@Text", MotivationDTO.Text);
            return ExecuteBool(command);
        }

        public bool DeleteMotivation(MotivationDTO MotivationDTO)
        {
            SqlCommand command = new SqlCommand("Delete FROM [dbo].[Motivation] WHERE MotivationID = @MotivationID", sqlConnection);
            command.Parameters.AddWithValue("@MotivationID", MotivationDTO.MotivationID);
            return ExecuteBool(command);
        }

        public MotivationDTO RandomPositiveMotivation()
        {
            SqlCommand command = new SqlCommand("SELECT TOP 1 * FROM [dbo].[Motivation] WHERE Positive = 1 ORDER BY NEWID()", sqlConnection);
            DataRow row = ExecuteRow(command);
            return new MotivationDTO(row["Text"].ToString(), Convert.ToBoolean(row["Positive"].ToString()));
        }

        public MotivationDTO RandomNegativeMotivation()
        {
            SqlCommand command = new SqlCommand("SELECT TOP 1 * FROM [dbo].[Motivation] WHERE Positive = 0 ORDER BY NEWID()", sqlConnection);
            DataRow row = ExecuteRow(command);
            return new MotivationDTO(row["Text"].ToString(), Convert.ToBoolean(row["Positive"].ToString()));
        }

        public List<MotivationDTO> GetAllMotivations()
        {
            Open();
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[Motivation]", sqlConnection);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            try
            {
                List<MotivationDTO> Motivation = new List<MotivationDTO>();

                foreach (DataRow row in table.Rows)
                {
                    MotivationDTO motivationDTO = new MotivationDTO(
                        Convert.ToInt32(row["MotivationID"]),
                        Convert.ToString(row["Text"]),
                        Convert.ToBoolean(row["Positive"])
                        );
                    Motivation.Add(motivationDTO);
                }
                return Motivation;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Close();
            }
        }

        public MotivationDTO GetSelectedMotivation(int MotivationID)
        {
            SqlCommand command = new SqlCommand("SELECT * FROM [dbo].[Motivation] WHERE MotivationID = @MotivationID", sqlConnection);
            command.Parameters.AddWithValue("@MotivationID", MotivationID);
            DataRow row = ExecuteRow(command);

            return new MotivationDTO(
                        Convert.ToInt32(row["MotivationID"]),
                        Convert.ToString(row["Text"]),
                        Convert.ToBoolean(row["Positive"]));
        }
    }
}

