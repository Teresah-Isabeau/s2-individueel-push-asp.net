﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushBAL
{
    public class Motivation
    {
        private IMotivation IMotivation;
       
        public int MotivationID { get; set; }
        public string Text { get; set; }
        public bool Positive { get; set; }

        public Motivation(IMotivation motivation)
        {
            this.IMotivation = motivation;
        }

        public Motivation(int motivationID, string text, bool positive)
        {
            MotivationID = motivationID;
            Text = text;
            Positive = positive;
        }

        private MotivationDTO MotivationToMotivationDTO(Motivation motivation)
        {
            return new MotivationDTO(motivation.MotivationID, motivation.Text, motivation.Positive);
        }

        public bool AddMotivation(IMotivation iMotivation)
        {
            return iMotivation.AddMotivation(MotivationToMotivationDTO(this));
        }

        public bool EditMotivation(IMotivation iMotivation)
        {
            return iMotivation.EditMotivation(MotivationToMotivationDTO(this));
        }

        public bool DeleteMotivation(IMotivation iMotivation)
        {
            return iMotivation.DeleteMotivation(MotivationToMotivationDTO(this));
        }

        public Motivation RandomPositiveMotivation()
        {
            MotivationDTO motivationDTO = IMotivation.RandomPositiveMotivation();
            this.MotivationID = motivationDTO.MotivationID;
            this.Text = motivationDTO.Text;
            this.Positive = motivationDTO.Positive;
            return this;
        }

        public Motivation RandomNegativeMotivation()
        {
            MotivationDTO motivationDTO = IMotivation.RandomNegativeMotivation();
            this.MotivationID = motivationDTO.MotivationID;
            this.Text = motivationDTO.Text;
            this.Positive = motivationDTO.Positive;
            return this;
        }

        public Motivation GetSelectedMotivation(int MotivationID)
        {
            MotivationDTO motivationDTO = IMotivation.GetSelectedMotivation(MotivationID);
            this.MotivationID = motivationDTO.MotivationID;
            this.Text = motivationDTO.Text;
            this.Positive = motivationDTO.Positive;
            return this;
        }
    }
}
