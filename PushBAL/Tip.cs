﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushBAL
{
    public class Tip
    {
        public int TipID { get; set; }
        public string tip { get; set; }

        public Tip(string tip)
        {
            this.tip = tip;
        }

        public Tip(int tipID, string tip)
        {
            this.TipID = tipID;
            this.tip = tip;
        }
    
        public Tip(int tipID)
        {
            TipID = tipID;
        }

        private TipDTO TipToTipDTO(Tip tip)
        {
            return new TipDTO(tip.tip);
        }

        public bool AddTip(ITip itip)
        {
            return itip.AddTip(TipToTipDTO(this));
        }
    }
}
