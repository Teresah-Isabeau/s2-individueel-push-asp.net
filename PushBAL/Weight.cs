﻿using System;
using System.Collections.Generic;
using System.Text;
using PushDAL;

namespace PushBAL
{
    public class Weight
    {
        private IWeight Iweight;

        public Weight(IWeight weight)
        {
            this.Iweight = weight;
        }

        public int UserID { get; set; }
        public int WeightID { get; set; }
        public double WeightAmount { get; set; }
        public DateTime WeightDate { get; set; }

        public Weight(int userid, double weightAmount, DateTime weightDate)
        {
            this.UserID = userid;
            this.WeightAmount = weightAmount;
            this.WeightDate = weightDate;
        }

        public Weight(int userid, int weightid, double weightAmount, DateTime weightDate)
        {
            this.UserID = userid;
            this.WeightID = weightid;
            this.WeightAmount = weightAmount;
            this.WeightDate = weightDate;
        }

        public Weight(double weightAmount, DateTime weightDate)
        {
            this.WeightAmount = weightAmount;
            this.WeightDate = weightDate;
        }

        public Weight(WeightDTO weightDTO)
        {
            this.UserID = weightDTO.UserID;
            this.WeightID = weightDTO.WeightID;
            this.WeightAmount = weightDTO.WeightAmount;
            this.WeightDate = weightDTO.WeightDate;
        }

        public Weight(int userID, int weightId)
        {
            UserID = userID;
            WeightID = weightId;
        }

        private WeightDTO WeightToWeightDTO(Weight weight)
        {
            return new WeightDTO(weight.UserID, weight.WeightAmount, weight.WeightDate);
        }

        public bool AddWeight(IWeight iWeight)
        {
            return iWeight.AddWeight(WeightToWeightDTO(this));
        }
    }
}
