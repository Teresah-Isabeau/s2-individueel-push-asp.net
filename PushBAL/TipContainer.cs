﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushBAL
{
    public class TipContainer
    {
        private ITipContainer ITipContainer;
        //private string tip;

        public TipContainer(ITipContainer itipContainer)
        {
            this.ITipContainer = itipContainer;
        }

        public TipContainer()
        {
        }

        private TipDTO TipToTipDTO(Tip tip)
        {
            return new TipDTO(tip.tip);
        }

        public bool EditTip(Tip tip)
        {
            return ITipContainer.EditTip(TipToTipDTO(tip));
        }

        public bool DeleteTip(Tip tip)
        {
            return ITipContainer.DeleteTip(TipToTipDTO(tip));
        }

        public Tip RandomTip()
        {
            TipDTO tipDTO = ITipContainer.RandomTip();
            return new Tip(tipDTO.Tip);
        }

        public List<Tip> GetAllTip()
        {
            List<TipDTO> tipsDTO = ITipContainer.GetAllTips();
            List<Tip> tipList = new List<Tip>();

            foreach (TipDTO tipDTO in tipsDTO)
            {
                tipList.Add(new Tip(tipDTO.Tip));
            }
            return tipList;
        }

        public Tip GetSelectedTip(int tipID)
        {
            TipDTO tipDTO = ITipContainer.GetSelectedTip(tipID);
            return new Tip(tipDTO.TipID, tipDTO.Tip);
        }
    }
}
