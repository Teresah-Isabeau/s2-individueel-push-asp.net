﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushBAL
{
    public class RecipeBook
    {
        private IRecipeBook IRecipeBook;
        public int RecipeBookID { get; set; }
        public string RecipeType { get; set; }

        public RecipeBook(IRecipeBook recipeBook)
        {
            this.IRecipeBook = recipeBook;
        }

        public RecipeBook(int recipebookID, string recipeType)
        {
            this.RecipeBookID = recipebookID;
            this.RecipeType = recipeType;
        }

        public RecipeBook GetSelectedRecipe(int recipebookID)
        {
            RecipeBookDTO recipeBookDTO = IRecipeBook.GetSelectedRecipe(recipebookID);
            this.RecipeBookID = recipeBookDTO.RecipeBookID;
            this.RecipeType = recipeBookDTO.RecipeType;
            return this;
        }
    }
}
