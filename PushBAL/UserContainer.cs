﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using PushDAL;

namespace PushBAL
{
    public class UserContainer
    {
        private IUserContainer IUserContainer;

        public UserContainer(IUserContainer userContainer)
        {
            this.IUserContainer = userContainer;
        }

        private UserDTO UserToUserDTO(User user)
        {
            return new UserDTO(user.UserID, user.Name, user.Email, user.Password, user.Length, user.Age, user.Gender, user.Admin, user.Goalweight);
        }

        public bool EditUser(User user)
        {
            return IUserContainer.EditUser(UserToUserDTO(user));
        }

        public bool DeleteUser(User user)
        {
            return IUserContainer.DeleteUser(UserToUserDTO(user));
        }

        public bool CheckUserExists(User user)
        {
            return IUserContainer.CheckUserExists(UserToUserDTO(user));
        }

        public bool CheckUniqueEmail(User user)
        {
            return IUserContainer.CheckUniqueEmail(UserToUserDTO(user));
        }

        public User FetchUser(User user)
        {
            UserDTO dt = IUserContainer.FetchUser(UserToUserDTO(user));

            return new User(dt.UserID, dt.Name, dt.Email, dt.Password, dt.Length, dt.Age, dt.Gender, dt.Admin, dt.Goalweight);
        }

        public List<User> GetAllUsers()
        {
            List<UserDTO> userDTOs = IUserContainer.GetAllUsers();
            List<User> userlist = new List<User>();

            foreach (UserDTO userDTO in userDTOs)
            {
                userlist.Add(new User(userDTO));
            }
            return userlist;
        }

        public bool ResetPassword(User user)
        {
            return IUserContainer.ResetPassword(UserToUserDTO(user));
        }

        public User GetSelectedUser(int userID)
        {
            UserDTO userDTO = IUserContainer.GetSelectedUser(userID);
            return new User(userDTO.UserID, userDTO.Name, userDTO.Email, userDTO.Length, userDTO.Age, userDTO.Gender, userDTO.Admin);           
        }
    }
}
