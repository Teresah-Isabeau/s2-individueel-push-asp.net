﻿using System;
using System.Collections.Generic;
using System.Text;
using PushDAL;

namespace PushBAL
{
    public class User
    {
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Length { get; set; }
        public DateTime Age { get; set; }
        public string Gender { get; set; }
        public int Admin { get; set; }
        public double Goalweight { get; set; } 

        public User(string email)
        {
            this.Email = email;
        }

        public User(int userID)
        {
            this.UserID = userID;
        }

        public User(string email, string password)
        {
            this.Email = email;
            this.Password = password;
        }

        public User(int userID, string password)
        {
            this.UserID = userID;
            this.Password = password;
        }

        public User(string name, string email, string password, int length)
        {
            this.Name = name;
            this.Email = email;
            this.Password = password;
            this.Length = length;
        }

        public User(int userid, string name, string email, string password, int length)
        {
            this.UserID = userid;
            this.Name = name;
            this.Email = email;
            this.Password = password;
            this.Length = length;
        }
        public User(int userid, string name, string email, int length, DateTime age, string gender, int admin)
        {
            this.UserID = userid;
            this.Name = name;
            this.Email = email;           
            this.Length = length;
            this.Age = age;
            this.Gender = gender;
            this.Admin = admin;
        }

        public User(int userid, string name, string email, string password, int length, DateTime age, string gender, int admin, double goalweight)
        {
            this.UserID = userid;
            this.Name = name;
            this.Email = email;
            this.Password = password;
            this.Length = length;
            this.Age = age;
            this.Gender = gender;
            this.Admin = admin;
            this.Goalweight = goalweight;
        }

        public User(UserDTO userDTO) 
        {
            this.UserID = userDTO.UserID;
            this.Name = userDTO.Name;
            this.Email = userDTO.Email;
            this.Password = userDTO.Password;
            this.Length = userDTO.Length;
            this.Age = userDTO.Age;
            this.Gender = userDTO.Gender;
            this.Admin = userDTO.Admin;
        }

        private UserDTO UserToUserDTO(User user)
        {
            return new UserDTO(user.Name, user.Email, user.Password, user.Length);
        }

        public bool AddUser(IUser iUser)
        {
            return iUser.AddUser(UserToUserDTO(this));
        }
    }
}
