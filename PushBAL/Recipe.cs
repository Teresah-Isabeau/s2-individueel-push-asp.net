﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushBAL
{
    public class Recipe
    {
        public int RecipeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int RecipeBookID { get; set; }
       
        public Recipe(int recipeID, string name, string description, int recipeBookID)
        {
            RecipeID = recipeID;
            Name = name;
            Description = description;
            RecipeBookID = recipeBookID;
        }

        public Recipe(int recipeID)
        {
            RecipeID = recipeID;
        }

        private RecipeDTO RecipeToRecipeDTO(Recipe recipe)
        {
            return new RecipeDTO(recipe.RecipeID, recipe.Name, recipe.Description, recipe.RecipeBookID);
        }

        public bool AddRecipe(IRecipe IRecipe)
        {
            return IRecipe.AddRecipe(RecipeToRecipeDTO(this));
        }
        public bool EditRecipe(IRecipe IRecipe)
        {
            return IRecipe.EditRecipe(RecipeToRecipeDTO(this));
        }
        public bool DeleteRecipe(IRecipe IRecipe)
        {
            return IRecipe.DeleteRecipe(RecipeToRecipeDTO(this));
        }
        public Recipe GetSelectedRecipe(IRecipe IRecipe, int recipeID)
        {
            //irecipe meegeven & this gebruiken
            RecipeDTO recipeDTO = IRecipe.GetSelectedRecipe(recipeID);
            this.RecipeID = recipeDTO.RecipeID;
            this.Name = recipeDTO.Name;
            this.Description = recipeDTO.Description;
            this.RecipeBookID = recipeDTO.RecipeBookID;
            return this;
        }
    }
}
