﻿using System;
using System.Collections.Generic;
using System.Text;
using PushDAL;

namespace PushBAL
{
    public class WeightContainer
    {
        private IWeightContainer IWeightContainer;

        public WeightContainer(IWeightContainer IweightContainer)
        {
            this.IWeightContainer = IweightContainer;  
        }

        private WeightDTO WeightToWeightDTO(Weight weight)
        {
            return new WeightDTO(weight.UserID, weight.WeightID, weight.WeightAmount, weight.WeightDate);
        }

        public bool EditWeight(Weight weight)
        {
            return IWeightContainer.EditWeight(WeightToWeightDTO(weight));
        }

        public bool DeleteWeight(Weight weight)
        {
            return IWeightContainer.DeleteWeight(WeightToWeightDTO(weight));
        }

        public List<Weight> GetAllWeight(int userID)
        {
            List<WeightDTO> weightsDTO = IWeightContainer.GetAllWeight(userID);
            List<Weight> weightList = new List<Weight>();

            foreach (WeightDTO weightDTO in weightsDTO)
            {
               // weightList.Add(new Weight(weightDTO.UserID, weightDTO.WeightID, weightDTO.WeightAmount, weightDTO.WeightDate));
                weightList.Add(new Weight(weightDTO));
            }
            return weightList;
        }

        public Weight GetFirstWeight(int userID)
        {
            WeightDTO weightDTO = IWeightContainer.GetFirstWeight(userID);
            return new Weight(userID, weightDTO.WeightID, weightDTO.WeightAmount, weightDTO.WeightDate);
        }

        public Weight GetLastWeight(int userID)
        {
            WeightDTO weightDTO = IWeightContainer.GetLastWeight(userID);
            return new Weight(userID, weightDTO.WeightID, weightDTO.WeightAmount, weightDTO.WeightDate);
        }

        public Weight GetSelectedWeight(int userID, int weightID)
        {
            WeightDTO weightDTO = IWeightContainer.GetSelectedWeight(userID, weightID);
            return new Weight(userID, weightDTO.WeightID, weightDTO.WeightAmount, weightDTO.WeightDate);
        }

        public Weight WeightResult(int userID)
        {
            WeightDTO weightDTO = IWeightContainer.WeightResult(userID);
            return new Weight(userID, weightDTO.WeightID, weightDTO.WeightAmount, weightDTO.WeightDate);
        }
    }
}
