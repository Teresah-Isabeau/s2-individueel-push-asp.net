﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushBAL
{
    public class RecipeBookContainer
    {
        private IRecipeBookContainer IRecipeBookContainer;

        public RecipeBookContainer(IRecipeBookContainer recipeBookContainer)
        {
            this.IRecipeBookContainer = recipeBookContainer;
        }      

        public List<RecipeBook> GetAllTypes()
        {
            List<RecipeBookDTO> recipeBookDTOs = IRecipeBookContainer.GetAllTypes();
            List<RecipeBook> recipebookList = new List<RecipeBook>();

            foreach(RecipeBookDTO recipebookDTO in recipeBookDTOs)
            {
                recipebookList.Add(new RecipeBook(recipebookDTO.RecipeBookID, recipebookDTO.RecipeType));
            }
            return recipebookList;
        }      
    }
}
