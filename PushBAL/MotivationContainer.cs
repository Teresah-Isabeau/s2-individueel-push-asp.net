﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushBAL
{
    public class MotivationContainer
    {
        private IMotivationContainer IMotivationContainer;
        public MotivationContainer(IMotivationContainer iMotivationContainer)
        {
            this.IMotivationContainer = iMotivationContainer;
        }

        public List<Motivation> GetAllMotivations()
        {
            List<MotivationDTO> motivationsDTO = IMotivationContainer.GetAllMotivations();
            List<Motivation> motivationList = new List<Motivation>();

            foreach (MotivationDTO MotivationDTO in motivationsDTO)
            {
                motivationList.Add(new Motivation(MotivationDTO.MotivationID, MotivationDTO.Text, MotivationDTO.Positive));
            }
            return motivationList;
        }
    }
}
