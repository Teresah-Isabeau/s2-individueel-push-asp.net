﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushBAL
{
    public class RecipeContainer
    {
        private IRecipeContainer IRecipeContainer;

        public RecipeContainer(IRecipeContainer recipeContainer)
        {
            this.IRecipeContainer = recipeContainer;
        }

        public List<Recipe> GetAllRecipe(int recipebookID)
        {
            List<RecipeDTO> recipeDTOs = IRecipeContainer.GetAllRecipes(recipebookID);
            List<Recipe> recipeList = new List<Recipe>();

            foreach (RecipeDTO recipeDTO in recipeDTOs)
            {
                recipeList.Add(new Recipe(recipeDTO.RecipeID, recipeDTO.Name, recipeDTO.Description, recipeDTO.RecipeBookID));
            }
            return recipeList;
        }   
    }
}
