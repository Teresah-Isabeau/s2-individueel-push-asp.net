﻿using NUnit.Framework;
using PushBAL;
using PushUnitTest.TestDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushUnitTest
{
    public class RecipeUnitTest
    {
        [Test]
        public void GetAllRecipes()
        {
            //arrange
            var RecipebookTestDAL = new RecipeBookTestDAL();
            var RecipebookContainer = new RecipeBookContainer(RecipebookTestDAL);

            //act
            List<RecipeBook> recipeBooks = RecipebookContainer.GetAllTypes();

            //assert
            Assert.AreEqual(4, recipeBooks.Count);
        }

        [Test]
        public void GetSelectedRecipe()
        {
            //arrange
            Recipe testRecipe = new Recipe(4, "name 4", "text 4", 4);

            var recipeTestDAL = new RecipeTestDAL();
            Recipe selectedRecipe = new Recipe(4);

            //act           
            var recipe = selectedRecipe.GetSelectedRecipe(recipeTestDAL, 4);

            //assert
            Assert.AreEqual(testRecipe.RecipeID, recipe.RecipeID);
            Assert.AreEqual(testRecipe.Name, recipe.Name);
            Assert.AreEqual(testRecipe.Description, recipe.Description);
            Assert.AreEqual(testRecipe.RecipeBookID, recipe.RecipeBookID);
        }
    }
}
