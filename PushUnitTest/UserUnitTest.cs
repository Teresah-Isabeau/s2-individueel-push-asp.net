﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Data;
using PushBAL;
using PushUnitTest.TestDAL;

namespace PushUnitTest
{
    public class UserUnitTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestAddUser()
        {
            // Arrange
            var userTestDAL = new UserTestDAL();            
            userTestDAL.ExistReturnValue = true;

            User user = new User("Test", "test@live.nl", "TEST", 179);

            // Act
            bool test = user.AddUser(userTestDAL);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void TestEditUser()
        {
            // Arrange
            var userTestDAL = new UserTestDAL();
            var userContainer = new UserContainer(userTestDAL);
            userTestDAL.ExistReturnValue = true;

            User user = new User("Test","test@live.nl", "TEST", 179);            

            // Act
            bool test = userContainer.EditUser(user);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void TestDeleteUser()
        {
            // Arrange
            var userTestDAL = new UserTestDAL();
            var userContainer = new UserContainer(userTestDAL);
            userTestDAL.ExistReturnValue = true;

            User user = new User(4);         

            // Act
            bool test = userContainer.DeleteUser(user);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void TestUserExists()
        {
            // Arrange
            var userTestDAL = new UserTestDAL();
            var userContainer = new UserContainer(userTestDAL);
            userTestDAL.ExistReturnValue = true;

            User user = new User("test@live.nl", "TEST");
            //UserContainer userContainer = new UserContainer(new UserTestDAL());

            // Act
            bool test = userContainer.CheckUserExists(user);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void TestEmailExists()
        {
            // Arrange
            var userTestDAL = new UserTestDAL();
            var userContainer = new UserContainer(userTestDAL);
            userTestDAL.ExistReturnValue = true;

            User user = new User("test@live.nl");
            //UserContainer userContainer = new UserContainer(new UserDAL());

            // Act
            bool test = userContainer.CheckUniqueEmail(user);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void TestGetAll()
        {
            //arrange
            var userTestDAL = new UserTestDAL();
            var userContainer = new UserContainer(userTestDAL);

            //act
            List<User> users = userContainer.GetAllUsers();

            //assert
            Assert.AreEqual(2 , users.Count);
        }

        [Test]
        public void TestFetchUser()
        {
            //arrange
            var testUser = new User(2, "Loes", "Loes@live.nl", "L03s", 178, default, "female", 0, 60);

            var selectedUser = new User(2);           

            var userTestDAL = new UserTestDAL();
            var userContainer = new UserContainer(userTestDAL);

            //act           
            var fetchedUser = userContainer.FetchUser(selectedUser);

            //assert
            Assert.AreEqual(testUser.UserID, fetchedUser.UserID);
            Assert.AreEqual(testUser.Name, fetchedUser.Name);
            Assert.AreEqual(testUser.Email, fetchedUser.Email);
            Assert.AreEqual(testUser.Gender, fetchedUser.Gender);   
        }
    }
}