﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushUnitTest.TestDAL
{
    class RecipeTestDAL : IRecipe, IRecipeContainer
    {
        public bool? ExistReturnValue = null;

        public List<RecipeDTO> recipes = new List<RecipeDTO>()
        {
            new RecipeDTO(1, "name 1", "text 1", 1),
            new RecipeDTO(2, "name 2", "text 2", 2),
            new RecipeDTO(3, "name 3", "text 3", 3),
            new RecipeDTO(4, "name 4", "text 4", 4)
        };

        public bool AddRecipe(RecipeDTO recipeDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public bool DeleteRecipe(RecipeDTO recipeDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public bool EditRecipe(RecipeDTO recipeDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public List<RecipeDTO> GetAllRecipes(int recipebookID)
        {
            return this.recipes;
        }

        public RecipeDTO GetSelectedRecipe(int recipeID)
        {
            return recipes.Find(x => x.RecipeID == recipeID);
        }
    }
}
