﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushUnitTest.TestDAL
{
    public class TipTestDAL : ITip, ITipContainer
    {
        public bool? ExistReturnValue = null;

        public List<TipDTO> tips = new List<TipDTO>()
        {
            new TipDTO(1, "tip 1"),
            new TipDTO(2, "tip 2"),
            new TipDTO(3, "tip 3"),
            new TipDTO(4, "tip 4"),
        };

        public bool AddTip(TipDTO tipDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }     

        public bool EditTip(TipDTO tipDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public bool DeleteTip(TipDTO tipDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public TipDTO RandomTip()
        {
            Random random = new Random();
            return tips[random.Next(0, 4)];
        }

        public TipDTO GetSelectedTip(int tipID)
        {
            return tips.Find(x => x.TipID == tipID);
        }

        public List<TipDTO> GetAllTips()
        {
            return this.tips;
        }
    }
}
