﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushUnitTest.TestDAL
{
    class MotivationTestDAL : IMotivation, IMotivationContainer
    {
        public bool AddMotivation(MotivationDTO MotivationDTO)
        {
            throw new NotImplementedException();
        }

        public bool DeleteMotivation(MotivationDTO MotivationDTO)
        {
            throw new NotImplementedException();
        }

        public bool EditMotivation(MotivationDTO MotivationDTO)
        {
            throw new NotImplementedException();
        }

        public List<MotivationDTO> GetAllMotivations()
        {
            throw new NotImplementedException();
        }

        public MotivationDTO GetSelectedMotivation(int MotivationID)
        {
            throw new NotImplementedException();
        }

        public MotivationDTO RandomNegativeMotivation()
        {
            throw new NotImplementedException();
        }

        public MotivationDTO RandomPositiveMotivation()
        {
            throw new NotImplementedException();
        }
    }
}
