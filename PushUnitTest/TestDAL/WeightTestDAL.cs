﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PushUnitTest.TestDAL
{
    public class WeightTestDAL : IWeight, IWeightContainer
    {
        public bool? ExistReturnValue = null;

        public List<WeightDTO> weights = new List<WeightDTO>()
        {
            new WeightDTO(1, 1, 90.5, default),
            new WeightDTO(1, 2, 91.0, default),
            new WeightDTO(1, 3, 89.8, default),
            new WeightDTO(2, 4, 79.8, default)
        };

        public bool AddWeight(WeightDTO weightDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public bool EditWeight(WeightDTO weightDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public bool DeleteWeight(WeightDTO weightDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public List<WeightDTO> GetAllWeight(int userID)
        {
            return weights.FindAll(x => x.UserID == userID);
        }

        public WeightDTO GetFirstWeight(int userID)
        {           
            return weights.First(x => x.UserID == userID);
        }

        public WeightDTO GetLastWeight(int userID)
        {
            return weights.Last(x => x.UserID == userID);
        }

        public WeightDTO GetSelectedWeight(int userID, int WeightID)
        {
            throw new NotImplementedException();
        }

        public WeightDTO WeightResult(int userID)
        {
            throw new NotImplementedException();
        }
    }
}
