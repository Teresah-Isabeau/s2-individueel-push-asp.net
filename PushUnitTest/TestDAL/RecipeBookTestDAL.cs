﻿using PushBAL;
using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushUnitTest.TestDAL
{
    class RecipeBookTestDAL : IRecipeBook, IRecipeBookContainer
    {
        public List<RecipeBookDTO> recipebooks = new List<RecipeBookDTO>()
        {
            new RecipeBookDTO(1, "type 1"),
            new RecipeBookDTO(2, "type 2"),
            new RecipeBookDTO(3, "type 3"),
            new RecipeBookDTO(4, "type 4")
        };

        public List<RecipeBookDTO> GetAllTypes()
        {
            return this.recipebooks;
        }

        public RecipeBookDTO GetSelectedRecipe(int recipebookID)
        {
            return recipebooks.Find(x => x.RecipeBookID == recipebookID);
        }
    }
}
