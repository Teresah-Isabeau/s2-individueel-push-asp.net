﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushUnitTest.TestDAL
{
    public class UserTestDAL : IUser, IUserContainer
    {
        public bool? ExistReturnValue = null;

        public List<UserDTO> users = new List<UserDTO>()
        {
            new UserDTO(1, "Kees", "Kees@live.nl", "K3es", 190, default, "men", 0, 0),
            new UserDTO(2, "Loes", "Loes@live.nl", "L03s", 178, default, "female", 0, 60)
        };

        public bool AddUser(UserDTO userDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public bool EditUser(UserDTO userDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public bool DeleteUser(UserDTO userDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public bool CheckUserExists(UserDTO userDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public bool CheckUniqueEmail(UserDTO userDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }

        public UserDTO FetchUser(UserDTO userDTO)
        {
            return users.Find(x => x.UserID == userDTO.UserID);
        }

        public List<UserDTO> GetAllUsers()
        {
            return this.users;
        }

        public UserDTO GetSelectedUser(int userID)
        {
            throw new NotImplementedException();
        }

        public bool ResetPassword(UserDTO userDTO)
        {
            if (ExistReturnValue == null)
            {
                throw new NullReferenceException("Invalid use of stub code. First set field ExistsReturnValue");
            }

            return ExistReturnValue.Value;
        }
    }
}
