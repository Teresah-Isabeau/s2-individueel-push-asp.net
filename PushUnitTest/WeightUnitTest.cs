﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Data;
using PushBAL;
using PushUnitTest.TestDAL;

namespace PushUnitTest
{
    class WeightUnitTest
    {
        [Test]
        public void TestAddWeight()
        {
            // Arrange
            var weightTestDAL = new WeightTestDAL();
            weightTestDAL.ExistReturnValue = true;


            Weight weight = new Weight(90, default);


            // Act
            bool test = weight.AddWeight(weightTestDAL);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void TestEditWeight()
        {
            // Arrange
            var weightTestDAL = new WeightTestDAL();
            var weightContainer = new WeightContainer(weightTestDAL);
            weightTestDAL.ExistReturnValue = true;

            Weight weight = new Weight(89, default);

            // Act
            bool test = weightContainer.EditWeight(weight);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void TestDeleteWeight()
        {
            // Arrange
            var weightTestDAL = new WeightTestDAL();
            var weightContainer = new WeightContainer(weightTestDAL);
            weightTestDAL.ExistReturnValue = true;

            Weight weight = new Weight(89, default);

            // Act
            bool test = weightContainer.DeleteWeight(weight);

            // Assert 
            Assert.IsTrue(test);
        }


        [Test]
        public void TestGetAllWeight()
        {
            // Arrange
            var weightTestDAL = new WeightTestDAL();
            var weightContainer = new WeightContainer(weightTestDAL);

            //var weight = new Weight();
            //weight.UserID = 1;



            // Act
            int weight = 1;
            List<Weight> weights = weightContainer.GetAllWeight(weight);

            // Assert
            Assert.AreEqual(3, weights.Count);
        }

        [Test]
        public void TestGetFirstWeight()
        {
            // Arrange
            var weightTestDAL = new WeightTestDAL();
            var weightContainer = new WeightContainer(weightTestDAL);


            // Act
            int userID = 1;
            Weight weight = weightContainer.GetFirstWeight(userID);

            // Assert
            Assert.AreEqual(1, weight.WeightID);
        }

        [Test]
        public void TestGetLastWeight()
        {
            // Arrange
            var weightTestDAL = new WeightTestDAL();
            var weightContainer = new WeightContainer(weightTestDAL);


            // Act
            int userID = 1;
            Weight weight = weightContainer.GetLastWeight(userID);

            // Assert
            Assert.AreEqual(3, weight.WeightID);
        }
    }
}
