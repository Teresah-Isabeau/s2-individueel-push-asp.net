﻿using NUnit.Framework;
using PushBAL;
using PushUnitTest.TestDAL;
using System;
using System.Collections.Generic;
using System.Text;

namespace PushUnitTest
{
    public class RecipeBookUnitTest
    {
        [Test]
        public void GetAllRecipeBooks()
        {
            //arrange
            var RecipebookTestDAL = new RecipeBookTestDAL();
            var RecipebookContainer = new RecipeBookContainer(RecipebookTestDAL);

            //act
            List<RecipeBook> recipeBooks = RecipebookContainer.GetAllTypes();

            //assert
            Assert.AreEqual(4, recipeBooks.Count);
        }

        [Test]
        public void GetSelectedRecipeBook()
        {
            //arrange
            RecipeBook testRecipeBook = new RecipeBook(2, "type 2");         

            var recipebookTestDAL = new RecipeBookTestDAL();
            RecipeBook selectedRecipeBook = new RecipeBook(recipebookTestDAL);

            //act           
            var recipebook = selectedRecipeBook.GetSelectedRecipe(2);

            //assert
            Assert.AreEqual(testRecipeBook.RecipeBookID, recipebook.RecipeBookID);
            Assert.AreEqual(testRecipeBook.RecipeType, recipebook.RecipeType);
        }
    }
}
