﻿using NUnit.Framework;
using System.Collections.Generic;
using System.Data;
using PushBAL;
using PushUnitTest.TestDAL;


namespace PushUnitTest
{
    public class TipUnitTest
    {
        [Test]
        public void TestAddTip()
        {
            // Arrange
            var tipTestDAL = new TipTestDAL();
            tipTestDAL.ExistReturnValue = true;

            Tip tip = new Tip("Test");

            // Act
            bool test = tip.AddTip(tipTestDAL);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void TestEditTip()
        {
            // Arrange
            var tipTestDAL = new TipTestDAL();
            var tipContainer = new TipContainer(tipTestDAL);
            tipTestDAL.ExistReturnValue = true;

            Tip tip = new Tip("Test");

            // Act
            bool test = tipContainer.EditTip(tip);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void TestDeleteTip()
        {
            // Arrange
            var tipTestDAL = new TipTestDAL();
            var tipContainer = new TipContainer(tipTestDAL);
            tipTestDAL.ExistReturnValue = true;

            Tip tip = new Tip("Test");

            // Act
            bool test = tipContainer.DeleteTip(tip);

            // Assert 
            Assert.IsTrue(test);
        }

        [Test]
        public void RandomTip()
        {
            //Arrage
            var tipTestDAL = new TipTestDAL();
            var tipContainer = new TipContainer(tipTestDAL);

            //act
            Tip tip = tipContainer.RandomTip();

            //assert 
            Assert.NotNull(tip);
        }

        [Test]
        public void GetSelectedTip()
        {
            //arrange
            var testTip = new Tip(2, "tip 2");

            int selectedTip = 2;


            var tipTestDAL = new TipTestDAL();
            var tipContainer = new TipContainer(tipTestDAL);

            //act           
            var Tip = tipContainer.GetSelectedTip(selectedTip);

            //assert
            Assert.AreEqual(testTip.TipID, Tip.TipID);
            Assert.AreEqual(testTip.tip, Tip.tip);     
        }

        [Test]
        public void GetAllTips()
        {
            //arrange
            var tipTestDAL = new TipTestDAL();
            var tipContainer = new TipContainer(tipTestDAL);

            //act
            List<Tip> tips = tipContainer.GetAllTip();

            //assert
            Assert.AreEqual(4, tips.Count);
        }
    }
}
