﻿using Microsoft.AspNetCore.Mvc;
using PushBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class UserViewModel
    {
        public int UserID { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public int Length { get; set; }

        public DateTime Age { get; set; }

        public string Gender { get; set; }

        public int Admin { get; set; }

        public UserViewModel(User user)
        {
            UserID = user.UserID;
            Name = user.Name;
            Email = user.Email;
            Length = user.Length;
            Age = user.Age;
            Gender = user.Gender;
            Admin = user.Admin;
        }

        public UserViewModel(int userID, string name, string email, int length, DateTime age, string gender, int admin)
        {
            UserID = userID;
            Name = name;
            Email = email;
            Length = length;
            Age = age;
            Gender = gender;
            Admin = admin;
        } 
    }
}
