﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class MotivationViewModel
    {
        public int MotivationID { get; set; }
        public string Text { get; set; }
        public bool Positive { get; set; }

        public MotivationViewModel()
        {

        }

        public MotivationViewModel(int motivationID)
        {
            this.MotivationID = motivationID;         
        }

        public MotivationViewModel(int motivationID, string text, bool positive)
        {
            this.MotivationID = motivationID;
            this.Text = text;
            this.Positive = positive;
        }
    }
}
