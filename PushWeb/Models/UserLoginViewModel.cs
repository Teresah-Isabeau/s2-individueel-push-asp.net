﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class UserLoginViewModel
    {
        [Required(ErrorMessage = "Required email")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Required password")]
        public string Password { get; set; }
    }
}
