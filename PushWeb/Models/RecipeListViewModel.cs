﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class RecipeListViewModel
    {
        public List<RecipeViewModel> lists;

        public RecipeListViewModel()
        {
            this.lists = new List<RecipeViewModel>();
        }
    }
}
