﻿using PushBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class RecipeViewModel
    {
        public int RecipeID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int RecipeBookID { get; set; }

        public RecipeViewModel(int recipeID, string name, string description, int recipebookID)
        {
            this.RecipeID = recipeID;
            this.Name = name;
            this.Description = description;
            this.RecipeBookID = recipebookID;
        }

        //public RecipeViewModel(Recipe recipe)
        //{
        //    this.RecipeID = recipe.RecipeID;
        //}

        public RecipeViewModel(Recipe recipe)
        {
            this.RecipeID = recipe.RecipeID;
            this.Name = recipe.Name;
            this.Description = recipe.Description;
            this.RecipeBookID = recipe.RecipeBookID;
        }
    }
}
