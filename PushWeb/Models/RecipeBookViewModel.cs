﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class RecipeBookViewModel
    {
        public int RecipeBookID { get; set; }
        public string RecipeType { get; set; }

        public RecipeBookViewModel()
        {

        }

        public RecipeBookViewModel(int recipebookID)
        {
            this.RecipeBookID = recipebookID;          
        }

        public RecipeBookViewModel(int recipebookID, string recipetype)
        {
            this.RecipeBookID = recipebookID;
            this.RecipeType = recipetype;
        }
    }
}
