﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class NewUserViewModel 
    {
        public int UserID { get; set; }

        [Required(ErrorMessage = "Required name")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        [Required(ErrorMessage = "Required email")]
        public string Email { get; set; }

        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Required password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Required length")]
        public int Length { get; set; }

        //voor BMI xpac
        public string Gender { get; set; }
        public DateTime DateofBirth { get; set; }

        //alleen voor admin
        public int Admin { get; set; }
    }
}
