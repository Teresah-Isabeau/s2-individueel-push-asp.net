﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class TipViewModel
    {
        public int TipID { get; set; }
        public string TipText { get; set; }

        public TipViewModel(int tipid, string tiptext)
        {
            this.TipID = tipid;
            this.TipText = tiptext;
        }

        public TipViewModel()
        {
        }
    }
}
