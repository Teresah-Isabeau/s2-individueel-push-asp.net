﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class MotivationListViewModel
    {
        public List<MotivationViewModel> lists;

        public MotivationListViewModel()
        {
            this.lists = new List<MotivationViewModel>();
        }
    }
}
