﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class HomeViewModel
    {
        public double FirstWeightAmount { get; set; }
        public double LastWeightAmount { get; set; }
        public double GoalWeightAmount { get; set; }
        public double WeightResult { get; set; }
        public string Tip { get; set; }
    }
}
