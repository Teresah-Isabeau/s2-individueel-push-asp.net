﻿using PushBAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class WeightListViewModel
    {
        public List<WeightViewModel> lists; 

        public WeightListViewModel()
        {
            this.lists = new List<WeightViewModel>();
        }
    }
}
