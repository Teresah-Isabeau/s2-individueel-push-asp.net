﻿using PushBAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class WeightViewModel
    {
        public int UserID { get; private set; }
        public int WeightID { get; set; }

        [Required]
        public double WeightAmount { get; set; }
        
        [Required]
        [DataType(DataType.Date)]
        public DateTime WeightDate { get; set; }
 

        public WeightViewModel()
        {
       
        }

        public WeightViewModel(double weightamount, DateTime weightdate)
        {        
            this.WeightAmount = weightamount;
            this.WeightDate = weightdate;
        }

        public WeightViewModel(int weightid, double weightamount, DateTime weightdate)
        {
            this.WeightID = weightid;
            this.WeightAmount = weightamount;
            this.WeightDate = weightdate;
        }

        public WeightViewModel(int weightId)
        {
            WeightID = weightId;
        }
    }
}
