﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class RecipeBookListViewModel
    {
        public List<RecipeBookViewModel> lists;

        public RecipeBookListViewModel()
        {
            this.lists = new List<RecipeBookViewModel>();
        }
    }
}
