﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class TipListViewModel
    {
        public List<TipViewModel> lists;

        public TipListViewModel()
        {
            this.lists = new List<TipViewModel>();
        }
    }
}
