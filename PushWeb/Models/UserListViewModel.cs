﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Models
{
    public class UserListViewModel
    {
        public int UserID { get; set; }

        public List<UserViewModel> lists;

        public UserListViewModel()
        {
            this.lists = new List<UserViewModel>();
        }
    }
}
