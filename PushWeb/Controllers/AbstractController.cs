﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Controllers
{
    public abstract class AbstractController : Controller
    {
        public IActionResult GetID(int? userID)
        {
            userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            return default; //userid?
        }
    }
}
