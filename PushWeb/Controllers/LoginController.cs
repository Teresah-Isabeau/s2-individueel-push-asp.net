﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PushWeb.Models;
using PushBAL;
using PushDAL;
using Microsoft.AspNetCore.Http;

namespace PushWeb.Controllers
{
    public class LoginController : Controller
    {
        UserContainer userContainer = new UserContainer(new UserDAL());

        [HttpGet]
        public IActionResult Index()
        {
            if (HttpContext.Session.GetInt32("userid").HasValue)
            {
                return RedirectToRoute(new { Controller = "Home", Action = "Index" });
            }
            return View();
        }

        [HttpPost] //Login
        public IActionResult Index(UserLoginViewModel ulvm)
        {
            if (ModelState.IsValid)
            {
                User user = new User(ulvm.Email, ulvm.Password);
                if (userContainer.CheckUserExists(user) == true)
                {
                    user = userContainer.FetchUser(user);

                    HttpContext.Session.SetInt32("userid", user.UserID);
                    HttpContext.Session.SetInt32("admin", user.Admin);

                    return RedirectToRoute(new { Controller = "Home", Action = "Index" });
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid!");
                }
            }         
                return View("Index");
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Register(NewUserViewModel nuvm)
        {
            if (ModelState.IsValid)
            {
                UserContainer userContainer = new UserContainer(new UserDAL());
                User user = new User(nuvm.Name, nuvm.Email, nuvm.Password, nuvm.Length);
                //alle textvelden moeten ingevult zijn

                if (userContainer.CheckUniqueEmail(user) == true)
                {
                    //Email exists
                    ModelState.AddModelError(string.Empty, "Email aleady exists!");
                    //return RedirectToAction("Register");
                }
                else
                {
                    user.AddUser(new UserDAL());
                    return RedirectToAction("Index");
                }             
            }
            return View("Register");
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();

            return RedirectToAction("Index");
        }


        [HttpGet]
        public IActionResult Reset()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Reset(UserViewModel uvm)
        {
            int? userID = HttpContext.Session.GetInt32("userid");
            if (userID.HasValue == false)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                User user = new User(userID.Value, uvm.Password);              
                userContainer.ResetPassword(user);

                return RedirectToAction("Index");
            }
        }
    }
}
