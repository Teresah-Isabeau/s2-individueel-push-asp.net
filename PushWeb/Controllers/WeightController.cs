﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PushBAL;
using PushDAL;
using PushWeb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace PushWeb.Controllers
{
    //[Authorize]
    public class WeightController : Controller
    {
        WeightContainer weightContainer = new WeightContainer(new WeightDAL());      

        [HttpGet]
        public IActionResult Index()
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            //!  userId heeft geen value 
            if (userID.HasValue == false)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }         

            List<Weight> weights = weightContainer.GetAllWeight(userID.Value);
            WeightListViewModel weightsViewModels = new WeightListViewModel();

            foreach (Weight item in weights)
            {
                weightsViewModels.lists.Add(new WeightViewModel(item.WeightID, item.WeightAmount, item.WeightDate));
            }
            return View(weightsViewModels);
        }

        [HttpGet]
        public IActionResult AddWeight()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddWeight(WeightViewModel wvm)
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            //!  userId heeft geen value 
            if (userID.HasValue == false)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                Weight weight = new Weight(
                    userID.Value,
                    Convert.ToDouble(wvm.WeightAmount),
                    wvm.WeightDate);//of DateTime.Now
                weight.AddWeight(new WeightDAL());

                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public IActionResult EditWeight(int weightid)
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                WeightViewModel wvm = new WeightViewModel(weightid);
                Weight weight = weightContainer.GetSelectedWeight(userID.Value, weightid);
                wvm.WeightAmount = weight.WeightAmount;
                wvm.WeightDate = weight.WeightDate;
                return View(wvm);
            }
            
        }

        [HttpPost]
        public IActionResult EditWeight(WeightViewModel wvm)
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                Weight weight = new Weight(
                    userID.Value,
                    wvm.WeightID,
                    wvm.WeightAmount,
                    wvm.WeightDate);
                
                weightContainer.EditWeight(weight);

                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public IActionResult DeleteWeight(int weightId)
        {

            int? userID = HttpContext.Session.GetInt32("userid");
            
            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                Weight weight = new Weight(userID.Value, weightId);                

                weightContainer.DeleteWeight(weight);

                return RedirectToAction("Index");
            }
        }
    }
}
