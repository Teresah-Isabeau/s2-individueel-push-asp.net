﻿using Microsoft.AspNetCore.Mvc;
using PushWeb.Models;
using PushBAL;
using PushDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PushWeb.Controllers
{
    public class UserController : Controller
    {
        UserContainer userContainer = new UserContainer(new UserDAL());

        [HttpGet]
        public IActionResult Index()
        {
            if (!HttpContext.Session.GetInt32("userid").HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            List<User> users = userContainer.GetAllUsers();
            UserListViewModel usersViewModels = new UserListViewModel();

            foreach (User item in users)
            {
                usersViewModels.lists.Add(new UserViewModel(item.UserID, item.Name, item.Email, item.Length, item.Age, item.Gender, item.Admin));
            }
            return View(usersViewModels);
        }

        [HttpGet]
        public IActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddUser(NewUserViewModel nuvm)
        {
            if (ModelState.IsValid)
            {              
                User user = new User(nuvm.Name, nuvm.Email, nuvm.Password, nuvm.Admin);
                //alle textvelden moeten ingevult zijn

                if (userContainer.CheckUniqueEmail(user) == true)
                {
                    //Email exists
                    ModelState.AddModelError(string.Empty, "Email aleady exists!");
                    //return RedirectToAction("Register");
                }
                else
                {
                    user.AddUser(new UserDAL());
                    return RedirectToAction("Index");
                }
            }
            return View("AddUser");
        }
    
        [HttpGet]
        public IActionResult EditUser()
        {                 
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {            
                User user = userContainer.GetSelectedUser(userID.Value);   
                UserViewModel uvm = new UserViewModel(user);
                return View(uvm);
            }            
        }

        [HttpPost]
        public IActionResult EditUser(UserViewModel uvm)
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                User user = new User(
                    uvm.UserID,
                    uvm.Name,
                    uvm.Email,
                    uvm.Length,
                    uvm.Age,
                    uvm.Gender,
                    uvm.Admin
                    );       

                userContainer.EditUser(user);
                
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public IActionResult DeleteUser()
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                User user = new User(userID.Value);
                userContainer.DeleteUser(user);

                return RedirectToAction("Index");
            }
        }
    }
}
