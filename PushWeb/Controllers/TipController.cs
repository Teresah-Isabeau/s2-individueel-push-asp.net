﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PushBAL;
using PushDAL;
using PushWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Controllers
{
    public class TipController : Controller
    {      
        TipContainer tipContainer = new TipContainer();

        [HttpGet]
        public IActionResult Index()
        {
            if (!HttpContext.Session.GetInt32("userid").HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            List<Tip> tips = tipContainer.GetAllTip();
            TipListViewModel tipListViewModels = new TipListViewModel();

           

            foreach (Tip item in tips)
            {
                tipListViewModels.lists.Add(new TipViewModel(item.TipID, item.tip));
            }
            return View(tipListViewModels);
        }

        [HttpGet]
        public IActionResult AddTip()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddTip(TipViewModel ntvm)
        {
            if (ModelState.IsValid)
            {
                Tip tip = new Tip(ntvm.TipText);

                tip.AddTip(new TipDAL());
                return RedirectToAction("Index");
                
            }
            return View("Register");
        }

        [HttpGet]
        public IActionResult EditTip(int tipID)
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                TipViewModel tvm = new TipViewModel();
                Tip tip = tipContainer.GetSelectedTip(tipID);
               
                tvm.TipID = tip.TipID;
                tvm.TipText = tip.tip;
                return View(tvm);
            }
        }

        [HttpPost]
        public IActionResult EditTip(TipViewModel tvm)
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                Tip tip = new Tip(
                    tvm.TipID,
                    tvm.TipText);
               

                tipContainer.EditTip(tip);

                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public IActionResult DeleteTip(int tipID)
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                Tip tip = new Tip(tipID);
                

                tipContainer.DeleteTip(tip);

                return RedirectToAction("Index");
            }
        }
    }
}
