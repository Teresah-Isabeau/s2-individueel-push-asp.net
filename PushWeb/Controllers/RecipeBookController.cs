﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PushBAL;
using PushDAL;
using PushWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Controllers
{
    public class RecipeBookController : Controller
    {
        RecipeBookContainer recipeBookContainer = new RecipeBookContainer(new RecipeBookDAL());

        [HttpGet]
        public IActionResult Index()
        {
            if (!HttpContext.Session.GetInt32("userid").HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }           
            else
            {
                List<RecipeBook> recipeBooks = recipeBookContainer.GetAllTypes();
                RecipeBookListViewModel recipeListViewModel = new RecipeBookListViewModel();
                
                foreach (RecipeBook item in recipeBooks)
                {
                    recipeListViewModel.lists.Add(new RecipeBookViewModel(item.RecipeBookID, item.RecipeType));                  
                }
                return View(recipeListViewModel);
            }
        }
    }
}
