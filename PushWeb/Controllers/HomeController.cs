﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PushBAL;
using PushDAL;
using PushWeb.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        TipDAL tip = new TipDAL();
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
       
        [HttpGet]
        public IActionResult Index()
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                WeightContainer weightContainer = new WeightContainer(new WeightDAL());
                HomeViewModel hvm = new HomeViewModel();
                Weight firstweight = weightContainer.GetFirstWeight(userID.Value);
                Weight lastweight = weightContainer.GetLastWeight(userID.Value);

                if (firstweight.WeightID == 0)
                {
                    hvm.FirstWeightAmount = 0;
                }
                else
                {
                    hvm.FirstWeightAmount = firstweight.WeightAmount;
                }
                if (lastweight.WeightID == 0)
                {
                    hvm.LastWeightAmount = 0;
                }
                else
                {
                    hvm.LastWeightAmount = lastweight.WeightAmount;
                }             

                //Deze moeten waarschijnlijk in de WeightController, amount 1 en 2 moeten met elkaar vergeleken worden voor het resultaat

                //Weight weightresult = weightContainer.WeightResult(userID.Value); 
                //hvm.WeightResult = weightresult.WeightAmount;

                TipContainer tipContainer = new TipContainer(new TipDAL()); 
                hvm.Tip = tipContainer.RandomTip().tip;



                return View(hvm);
            }
            
        }       

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
