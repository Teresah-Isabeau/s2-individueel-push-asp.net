﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Controllers
{
    public class AdminController : Controller
    {
        public IActionResult Admin()
        {
            int? userID = HttpContext.Session.GetInt32("userid");

            if (!userID.HasValue || HttpContext.Session.GetInt32("is_admin").Value == 0)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {               
                return View();
            }

        }
    }
}
