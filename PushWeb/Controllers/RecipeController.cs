﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PushBAL;
using PushDAL;
using PushWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushWeb.Controllers
{
    public class RecipeController : Controller
    {
        RecipeContainer recipeContainer = new RecipeContainer(new RecipeDAL());

        [HttpGet]
        public IActionResult List(int recipebookID)
        {
            if (!HttpContext.Session.GetInt32("userid").HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                List<Recipe> recipes = recipeContainer.GetAllRecipe(recipebookID);
                RecipeListViewModel recipeListViewModel = new RecipeListViewModel();

                foreach (Recipe item in recipes)
                {
                    recipeListViewModel.lists.Add(new RecipeViewModel(item.RecipeID, item.Name, item.Description, item.RecipeBookID));
                }
                return View(recipeListViewModel);
            }
        }

        [HttpGet]
        public IActionResult Details(int recipeID)
        {
            if (!HttpContext.Session.GetInt32("userid").HasValue)
            {
                return RedirectToRoute(new { Controller = "Login", Action = "Index" });
            }
            else
            {
                Recipe recipe = new Recipe(recipeID);
                recipe= recipe.GetSelectedRecipe(new RecipeDAL(),recipeID);
                RecipeViewModel rvm = new RecipeViewModel(recipe);

                return View(rvm);                
            }
        }
    }
}
